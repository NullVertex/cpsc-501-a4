type Complex = (f64, f64);

pub fn pad_inputs(i1: &[f64], i2: &[f64]) -> (Vec<Complex>, Vec<Complex>)
{
    let mut longer: usize = 0;
    if i1.len() >= i2.len()
    {
        longer = i1.len();
    }
    else
    {
        longer = i2.len();
    }

    let new_length: usize  = next_power_of_2(2 * longer as i32) as usize;
    let mut r1 = vec![(0.0, 0.0); new_length];
    let mut r2 = vec![(0.0, 0.0); new_length];

    for i in 0..i1.len()
    {
        r1[i] = (i1[i], 0.0);
    }

    for j in 0..i2.len()
    {
        r2[j] = (i2[j], 0.0);
    }

    (r1, r2)
}

/// Calcultes FFT. use a sign of 1.0 for normal FFT and -1.0 for inverse fft
pub fn fft(input: &[Complex]) -> Vec<Complex>
{
    let pi: f64 = 3.141592653589793238460;

    let n = input.len();

    let mut x = vec![(0.0, 0.0); n];

    if n > 1
    {
        let mut e = vec![(0.0, 0.0); n / 2];
        let mut o = vec![(0.0, 0.0); n / 2];

        for i in 0..(n / 2)
        {
            e[i] = input[2 * i];
            o[i] = input[(2 * i) + 1];
        }

        let even = fft(e.as_slice());
        let odd  = fft(o.as_slice());

        for k in 0..(n/2)
        {
            let t = mul( polar(1.0, -2.0 * pi * k as f64 / n as f64), odd[k] );

            x[k        ] = add( even[k], t );
            x[k + (n/2)] = sub( even[k], t );
        }

        x
    }
    else
    {
        x[0] = input[0];

        x  
    }
}

pub fn ifft(input: &[Complex]) -> Vec<Complex>
{
    let n = input.len();
    let mut swap = vec![(0.0, 0.0); input.len()];
    for i in 0..input.len()
    {
        swap[i] = (input[i].1, input[i].0);
    }

    let mut fft_result: Vec<Complex> = fft(swap.as_slice());

    for i in 0..fft_result.len()
    {
        fft_result[i] = (fft_result[i].1 / n as f64, fft_result[i].0 / n as f64);
    }

    fft_result
}

fn polar(rho: f64, theta: f64) -> Complex
{
    let re: f64 = rho * f64::cos(theta);
    let im: f64 = rho * f64::sin(theta);

    (re, im)
}

pub fn mul(c1: Complex, c2: Complex) -> Complex
{
    // (a + ib) (c + id)
    // real = ac - bd
    let re = (c1.0 * c2.0) - (c1.1 * c2.1);
    // imaginary = ad + cb
    let im = (c1.0 * c2.1) + (c1.1 * c2.0);

    (re, im)
}

fn add(c1: Complex, c2: Complex) -> Complex
{
    // (a + ib) + (c + id)
    // real = a + c
    let re = c1.0 + c2.0;
    // imaginary = i(b + d)
    let im = c1.1 + c2.1;

    (re, im)
}

fn sub(c1: Complex, c2: Complex) -> Complex
{
    // (a + ib) - (c + id)
    // real = a - c
    let re = c1.0 - c2.0;
    // imaginary = i(b - d)
    let im = c1.1 - c2.1;

    (re, im)
}

// Finds the next power of 2 so that the input is the right length for our FFT algorithm
fn next_power_of_2(i: i32) -> i32
{
    let mut k: i32 = 1;
    while k < i
    {
        // Cheap substitution for *= 2
        k = k << 1;
    }

    k
}