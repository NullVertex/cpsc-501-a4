extern crate fft;

use std::env;
use std::fs::File;
use std::io;
use std::io::prelude::*;

type Complex = (f64, f64);

fn main()
{
    let arguments: Vec<String> = env::args().collect();

    if arguments.len() == 4
    {
        match read_as_bytes(env::args().nth(1).unwrap())
        {
            Ok(x) =>
            {
                match read_as_bytes(env::args().nth(2).unwrap())
                {
                    Ok(h) =>
                    {
                        println!("\nMETADATA FOR: {}", arguments[1]);
                        let signal_x = convert(x);
                        println!("\nMETADATA FOR: {}", arguments[2]);
                        let signal_h = convert(h);
                        
                        println!("\nPerforming the convolution... This may take a while.");
                        let signal_y = fft_convolve(signal_x.as_slice(), signal_h.as_slice());

                        match write_wav(env::args().nth(3).unwrap(), signal_y)
                        {
                            Ok(_) => println!("Successfully wrote file: \"{}\"", arguments[3]),
                            Err(e) => println!("Writing output file \"{}\" failed with the following error: \n{:?}", arguments[3], e),
                        }
                    },
                    Err(e) => println!("Error: \n{:?}", e),
                }
            },
            Err(e) => println!("Error: \n{:?}", e),
        }
    }
    else
    {
        println!("Usage: convolve inputfile IRfile outputfile");
    }
}

/// A Time-domain implementation of the convolution algorithm
fn td_convolve(x: &[f64], h: &[f64]) -> Vec<f64>
{
    let length = x.len() + h.len();
    let mut y = vec![0.0f64; length - 1];

    for n in 0..x.len()
    {
        for m in 0..h.len()
        {
            y[n + m] += x[n] * h[m];
        }
    }

    // Scale back the signal so that it falls between the -1 to 1 range
    let mut max: f64 = 0.0;
    for i in 0..y.len()
    {
        if y[i].abs() > max
        {
            max = y[i].abs();
        }
    }

    for j in 0..y.len()
    {
        y[j] = y[j] /  max;
    }

    y
}

/// A frequency domain implementation of the convolution algorithm
fn fft_convolve(x: &[f64], h: &[f64]) -> Vec<f64>
{
    // Final length of output signal
    let final_len = x.len() + h.len() - 1;
    // Pad signals to same length which is a power of 2 for fft
    let (x_pad, h_pad) = fft::pad_inputs(x, h);
    // Perform fft on each signal
    let xk = fft::fft(x_pad.as_slice());
    let hk = fft::fft(h_pad.as_slice());

    // Holds the convoluted signal
    let mut freq_y = vec![(0.0, 0.0); xk.len()];

    // Multiply the signal's spectra
    for i in 0..xk.len()
    {
        freq_y[i] = fft::mul( xk[i], hk[i]);
    }

    // inverse the FFT to get back time domain
    let time_y = fft::ifft(freq_y.as_slice());
    let mut y = vec![0.0; final_len];

    let mut max: f64 = 0.0;
    // Strip the imaginary part of the signal
    for j in 0..final_len
    {
        y[j] = time_y[j].0;
        if y[j].abs() > max
        {
            max = y[j].abs();
        }
    }

    for l in 0..final_len
    {
        y[l] /= max;
    }

    y
}

/// Converts a raw byte vector representing the wav file into a vector of floating point values, so we can process the convolution
fn convert(data: Vec<u8>) -> Vec<f64>
{
    // Print some metadata so we know what's going on
    let meta_size = &data[16..20];
    let meta_size_val = to_i32(meta_size);
    println!("META CHUNK SIZE: {}", meta_size_val);

    let num_channels = &data[22..24];
    let num_channels_val = to_i16(num_channels);
    println!("NUMBER OF CHANNELS: {}", num_channels_val);

    let sample_rate = &data[24..28];
    let sample_rate_val = to_i32(sample_rate);
    println!("SAMPLE RATE: {} Hz", sample_rate_val);

    let bits_per_sample = &data[34..36];
    let bits_per_sample_val = to_i16(bits_per_sample);
    println!("BYTE RATE: {}", sample_rate_val * num_channels_val as i32 * bits_per_sample_val as i32 / 8);

    let block_size_val = num_channels_val as i32 * bits_per_sample_val as i32 / 8;
    println!("BLOCK ALIGN: {}", block_size_val);
    println!("BITS PER SAMPLE: {}", bits_per_sample_val);

    // Offset for start of samples is 12 for RIFF Chunk + 8 for Sub-chunk 2 + size of meta chunk
    let sample_start_addr: i32 = 20 + meta_size_val;
    let samples = &data[(sample_start_addr as usize)..];

    let num_samples_val = samples.len() as i32 / block_size_val;
    println!("NUMBER OF SAMPLES: {}", num_samples_val);
    println!("Duration: {:.3}s", num_samples_val as f64 / sample_rate_val as f64);

    // Each sample should be two bytes, so the resulting vector should only be half as long as the original
    let length = samples.len() / 2;
    let mut x = vec![0.0f64; length];

    for i in 0..length
    {
        let current_offset: usize = (i * 2) as usize;
        let current_slice = &[samples[current_offset], samples[current_offset + 1]];
        x[i] = (to_i16(current_slice) as f64) / (i16::max_value() as f64);
    }

    x
}

/// Reads the raw contents of the file into a Vector of bytes
fn read_as_bytes(filename: String) -> Result<Vec<u8>, io::Error>
{
    let file = try!(File::open(filename));
    let mut bytestream: Vec<u8> = Vec::new();
    for byte in file.bytes()
    {
        bytestream.push(byte.unwrap());
    }
    Ok(bytestream)
}

/// Converts a 4 byte slice to a 32 bit signed integer (assuming little endian, per the WAV standard)
fn to_i32(slice: &[u8]) -> i32
{
    let mut value = 0;
    value += (slice[3] as u32) << 24;
    value += (slice[2] as u32) << 16;
    value += (slice[1] as u32) << 8;
    value += slice[0] as u32;

    value as i32 // convert back to signed integer
}

/// Converts a 2 byte slice to a 16 bit signed integer (assuming little endian, per the WAV standard)
fn to_i16(slice: &[u8]) -> i16
{
    let mut value = 0;
    value += (slice[1] as u16) << 8;
    value += slice[0] as u16;

    value as i16 // convert back to signed integer
}

/// Converts a raw i16 into a 2-byte little endian value that can be written to disk
fn i16_to_le(i: i16) -> [u8; 2]
{
    let s_0: u8 = i as u8 ^ 0u8;
    let s_1: u8 = (i >> 8) as u8 ^ 0u8;

    let s = [s_0, s_1];

    s
}

/// Convert a raw i32 into a 4-byte little endian value that can be written to disk
fn i32_to_le(i: i32) -> [u8; 4]
{
    // Inverse of to_i32
    let s_0: u8 = i as u8 ^ 0u8;
    let s_1: u8 = (i >> 8) as u8 ^ 0u8;
    let s_2: u8 = (i >> 16) as u8 ^ 0u8;
    let s_3: u8 = (i >> 24) as u8 ^ 0u8;

    // Construct the slice
    let s = [s_0, s_1, s_2, s_3];

    s
}

/// Will write a WAV file to disk, converting a list of f64 samples into i16 values in the process (little endian assumed)
fn write_wav(filename: String, samples: Vec<f64>) -> Result<(), std::io::Error>
{
    let mut f = try!(File::create(filename));
    try!(f.write(b"RIFF"));
    // Remember to double the sample length as theres 2 bytes to each sample!
    let filesize: i32 = 36 + (samples.len() * 2) as i32;
    try!(f.write(&i32_to_le(filesize)));
    try!(f.write(b"WAVE"));
    try!(f.write(b"fmt "));
    // Meta size is 16 bytes
    try!(f.write(&i32_to_le(16i32)));
    // Encoded as PCM_s16le
    try!(f.write(&i16_to_le(1i16)));
    // 1 Channel
    try!(f.write(&i16_to_le(1i16)));
    // Sample Rate
    try!(f.write(&i32_to_le(44100i32)));
    // Byte Rate
    try!(f.write(&i32_to_le(88200i32)));
    // Block Align
    try!(f.write(&i16_to_le(2i16)));
    // Bits per sample
    try!(f.write(&i16_to_le(16i16)));
    try!(f.write(b"data"));
    // Remember to double the sample length as theres 2 bytes to each sample!
    try!(f.write(&i32_to_le((samples.len() * 2) as i32)));
    for i in 0..samples.len()
    {
        let sample: i16 = (samples[i] * (i16::max_value() - 1) as f64) as i16;
        try!(f.write(&i16_to_le(sample)));
    }

    Ok(())
}
