rustc -g --crate-type=lib fft.rs -C opt-level=3
rustc convolve.rs -g --extern fft=libfft.rlib -C opt-level=3