# CPSC 501 - A4
This program allows for convolution of .wav files  
usage: `convolve inputfile IRfile outputfile`

# Analysis
## Methodology
Compiled with the most recent version of the rust compiler (1.13.0 as of writing)  
All tests conducted on:  
Intel Core i5 4670k (3.4 GHz, 4 Cores, 4 Threads)  
  
Execution time was computed with the `Measure-Command` command in Windows Powershell
The difference between the output was calculated by using the unix command `xxd` to produce raw hex dumps of the wav files, which were then compared with the `diff` command. 

## Baseline program (Time domain convolution)
Convolving a 30s audio clip with the 3.588s Taj Mahal impulse response
<table>
    <tr>
        <th></th>
        <th>opt-level=0</th>
        <th>opt-level=1</th>
        <th>opt-level=2</th>
        <th>opt-level=3</th>
    </tr>
    <tr>
        <th>Execution time (s)</th>
        <td>7602.35</td>
        <td>2360.64</td>
        <td>154.63</td>
        <td>136.95</td>
    </tr>
    <tr>
        <th>Difference (%)</th>
        <td>N/A (Baseline)</td>
        <td>0</td>
        <td>0</td>
        <td>0</td>
    </tr>
</table>

Using the compiler optimization did not seem to have any impact on the output (luckily)  
However, the impact on speed in this case is staggering. The opt-level=3 result produced code that executed 55.5 times faster than the unoptimized version

## Algorithm-optimized program (Frequency domain convolution)
Convolving the same 30s audio clip and Taj Mahal impulse response as before gives the following result:
<table>
    <tr>
        <th></th>
        <th>opt-level=0</th>
        <th>opt-level=1</th>
        <th>opt-level=2</th>
        <th>opt-level=3</th>
    </tr>
    <tr>
        <th>Execution time (s)</th>
        <td>61.65</td>
        <td>33.03</td>
        <td>14.40</td>
        <td>13.89</td>
    </tr>
    <tr>
        <th>Difference (%)</th>
        <td>0</td>
        <td>0</td>
        <td>0</td>
        <td>0</td>
    </tr>
</table>

During the debugging process of switching my program I did notice some slight rounding errors when using the FFT, however they were small enough that when converted back to signed 16-bit integers that the output was bitwise identical to the original. However, the performance difference was astronomical. With compiler optimization turned off, the new program was 123.3 times as fast as the original for this sample. With compiler optimization on, we still see the new program was nearly 10 times faster in processing this sample.

## Hand-tuned program (Final version)
Convolving the same 30s audio clip and Taj Mahal impulse response as before gives the following result:
<table>
    <tr>
        <th></th>
        <th>opt-level=0</th>
        <th>opt-level=1</th>
        <th>opt-level=2</th>
        <th>opt-level=3</th>
    </tr>
    <tr>
        <th>Execution time (s)</th>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <th>Difference (%)</th>
        <td>0</td>
        <td>0</td>
        <td>0</td>
        <td>0</td>
    </tr>
</table>

Used `cargo profiler` (based off of valgrind) to analyze the program's performance.